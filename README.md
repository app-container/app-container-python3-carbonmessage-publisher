poky-app-container-python3-nmap-srv
=======================

build local container
=====================

```
pushd local_scripts/
```
```
./docker_build.sh
```
```
popd
```

run local container with ash
============================

```
pushd local_scripts/
```

```
./docker_run-ash.sh reslocal/poky-app-container-nmap
```

```
...
+ ID=$(docker run -t -i -d reslocal/poky-app-container-python3-nmap-srv ash -l)
+ ID 665f7286406be16143011434d4d4a30cc2d242c654686f25ecb59aae0fab0c3b
+ docker attach 665f7286406be16143011434d4d4a30cc2d242c654686f25ecb59aae0fab0c3b
...
```


```
root@665f7286406b:/# python3
Python 3.7.2 (default, May 15 2019, 07:43:17)
[GCC 8.3.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

```
CTRL+D
```

```
root@665f7286406b:/# nmap -v -sn 192.168.42.1
Starting Nmap 7.70 ( https://nmap.org ) at 2019-05-17 16:22 UTC
Initiating Ping Scan at 16:22
Scanning 192.168.42.1 [4 ports]
Completed Ping Scan at 16:22, 0.21s elapsed (1 total hosts)
Initiating Parallel DNS resolution of 1 host. at 16:22
Completed Parallel DNS resolution of 1 host. at 16:22, 0.00s elapsed
Nmap scan report for e450-1.res.training (192.168.42.1)
Host is up (0.000065s latency).
Read data files from: /usr/bin/../share/nmap
Nmap done: 1 IP address (1 host up) scanned in 0.25 seconds
           Raw packets sent: 4 (152B) | Rcvd: 1 (28B)
root@665f7286406b:/# 
```

```
root@38cb4e887907:/# exit
```

```
popd
```


run without ash
===============

```
pushd local_scripts/
```
```
./docker_run.sh reslocal/poky-app-container-python3-nmap-srv nmap -v -sn 192.168.42.1
```

```
...
+ ID=$(docker run -t -i reslocal/poky-app-container-python3-nmap-srv nmap -v -sn 192.168.42.1)
+ ID Starting Nmap 7.70 ( https://nmap.org ) at 2019-05-17 16:24 UTC
Initiating Ping Scan at 16:24
Scanning 192.168.42.1 [4 ports]
Completed Ping Scan at 16:24, 0.22s elapsed (1 total hosts)
Initiating Parallel DNS resolution of 1 host. at 16:24
Completed Parallel DNS resolution of 1 host. at 16:24, 0.00s elapsed
Nmap scan report for e450-1.res.training (192.168.42.1)
Host is up (0.000080s latency).
Read data files from: /usr/bin/../share/nmap
Nmap done: 1 IP address (1 host up) scanned in 0.25 seconds
           Raw packets sent: 4 (152B) | Rcvd: 1 (28B)
```
```
popd
```

